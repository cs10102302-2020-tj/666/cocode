"use strict";

import axios from "axios";

const _axios = axios.create({
    withCredentials: true,
});
_axios.defaults.headers.post["Content-Type"] = "application/json";

export default _axios;
