"use strict";

// 应用mutation
export const setUser = ({ commit }, user) => {
    commit("userStatus", user);
};

export const setUserToken = ({ commit }, userToken) => {
    commit("userToken", userToken);
};

// 设置用户头像
export const setUserAvatar = ({ commit }, userAvatar) => {
    commit("userAvatar", userAvatar);
};

export const setUserName = ({ commit }, userName) => {
    commit("userName", userName);
};

export const setNickName = ({ commit }, nickName) => {
    commit("nickName", nickName);
};

export const setUserColor = ({ commit }, userColor) => {
    commit("userColor", userColor);
};

export const setProjectName = ({ commit }, projectName) => {
    commit("projectName", projectName);
};
