"use strict";

import io from "socket.io-client";

// todo: 加入code 和 chat namespace，便于后续区分其它通讯要求
class SocketIO {
    constructor(apiUrl, evtbus) {
        this.socket = null;
        this.apiUrl = apiUrl;
        this.evtbus = evtbus;

        this.connect();

        // 订阅的代码编辑加入room事件
        this.evtbus.$on("CodeEdit.JoinRoom", data => this.JoinRoom(data));

        this.evtbus.$on("CodeEdit.UserLeave", data => this.UserLeaveCodeEdit(data));

        // 订阅的代码更新同步事件
        this.evtbus.$on("CodeEdit.CodeRefresh", data => this.CodeRefresh(data));

        // 订阅的代码滚动同步事件
        this.evtbus.$on("CodeEdit.CodeScroll", data => this.CodeScroll(data));

        // 订阅的聊天发布消息事件
        this.evtbus.$on("CodeChat.SendMessage", msg => this.SendChatMessage(msg));

        // 订阅的申请切换编辑角色消息
        this.evtbus.$on("CodeEdit.ChangeEditRoleReq", msg => this.ChangeEditRoleReq(msg));

        // 订阅的确认切换编辑角色消息
        this.evtbus.$on("CodeEdit.ChangeEditRoleReply", msg => this.ChangeEditRoleReply(msg));

        // 订阅的邀请用户编辑消息
        this.evtbus.$on("CodeEdit.InviteUserCodeReq", msg => this.InviteUserCodeReq(msg));

        // 订阅的确认切换编辑角色消息
        this.evtbus.$on("CodeEdit.InviteUserCodeReply", msg => this.InviteUserCodeReply(msg));

        // 订阅用户通知后端在线消息
        this.evtbus.$on("User.TouchServer", data => this.UserTouchServer(data));

        // 订阅用户通知后端在线消息
        this.evtbus.$on("CodeEdit.UserTouchServer", data => this.CodeEditUserTouchServer(data));

        // 订阅通知房间内人员有人加入消息
        this.evtbus.$on("CodeEdit.NotifyUserJoin", (roomId, user) => {
            this.CodeEditNotifyUserJoin(roomId, user);
        });

        this.evtbus.$on(
            "CodeEdit.UserLeaveRoom",
            (roomId, user, creatorChangeNotify, editorChangeNotify, newUser) => {
                this.CodeEditUserLeaveRoom(
                    roomId,
                    user,
                    creatorChangeNotify,
                    editorChangeNotify,
                    newUser,
                );
            },
        );

        this.evtbus.$on("CodeEdit.SaveFile", (projectName, filePath, roomId, userName, fileContent) =>
            this.CodeEditSaveFile(projectName, filePath, roomId, userName, fileContent),
        );

        this.evtbus.$on("Filetree.SendUpdate", (data) =>
            this.FiletreeSendUpdate(data),
        );
    }

    connect() {
        // 触发socket连接
        if (!this.socket) {
            this.socket = io(`${this.apiUrl}/`);

            // 接收到的服务器端的char msg处理
            this.socket.on("receiveMessage", msg => this.ReceiveChatMessage(msg));

            // 接收到的服务器端的room里的用户列表
            this.socket.on("userList", users => this.getCodeUsers(users));

            // 接收到的服务器端的有人离开消息
            this.socket.on("othersLeave", msg => this.sendCodeLeaveMsg(msg));

            this.socket.on("codeUpdate", codeContent => this.CodeUpdate(codeContent));

            this.socket.on("editorScrollUpdate", codeContent =>
                this.CodeEditorScrollUpdate(codeContent),
            );

            this.socket.on("changeEditRoleReqTransfer", msg =>
                this.codeEditChangeEditReqRoleServerTransfer(msg),
            );

            this.socket.on("changeEditRoleReplyTransfer", msg =>
                this.codeEditChangeEditRoleReplyServerTransfer(msg),
            );

            this.socket.on("inviteUserCodeReqTransfer", msg =>
                this.codeEditInviteUserCodeReqServerTransfer(msg),
            );

            this.socket.on("inviteUserCodeReplyTransfer", msg =>
                this.codeEditInviteUserCodeReplyServerTransfer(msg),
            );

            this.socket.on("codeEditNotifyUserJoinReqTransfer", usr =>
                this.codeEditHasUserJoinCodeEdit(usr),
            );

            this.socket.on("codeEditUserLeaveRoomReqTransfer", data =>
                this.codeEditUserLeaveRoom(data),
            );

            this.socket.on("filetreeUpdateTransfer", () => {
                this.FiletreeUpdate();
            });

            this.socket.on("codeEditMsg", (msg) => {
                this.SendServerMsg(msg);
            });

            this.socket.on("codeEditProjectUserRemoveMsg", (msg) => {
                this.SendServerProjectUserRemoveMsg(msg);
            });

            this.socket.on("codeEditProjectRemoveMsg", (msg) => {
                this.SendServerProjectRemoveMsg(msg);
            });
        }
    }

    JoinRoom(data) {
        this.connect();
        this.socket.emit("join", {
            userId: data.userId,
            userName: data.userName,
            room: data.room,
        });
    }

    UserTouchServer(data) {
        this.connect();
        this.socket.emit("userTouch", data);
    }

    CodeEditUserTouchServer(data) {
        this.connect();
        this.socket.emit("codeEditUserTouch", data);
    }

    CodeEditNotifyUserJoin(roomId, joinUser) {
        this.connect();
        this.socket.emit("codeEditNotifyUserJoinReq", {
            roomId: roomId,
            user: joinUser,
        });
    }

    CodeEditUserLeaveRoom(roomId, leaveUser, creatorChangeNotify, editorChangeNotify, newUser) {
        this.connect();
        this.socket.emit("codeEditUserLeaveRoomReq", {
            roomId: roomId,
            user: leaveUser,
            creatorChangeNotify: creatorChangeNotify,
            editorChangeNotify: editorChangeNotify,
            newUser: newUser,
        });
    }

    CodeEditSaveFile(projectName, filePath, roomId, userName, fileContent) {
        this.connect();
        this.socket.emit("codeEditSaveFile", {
            projectName: projectName,
            filePath: filePath,
            roomId: roomId,
            userName: userName,
            content: fileContent,
        });
    }

    UserLeaveCodeEdit(data) {
        this.connect();
        this.socket.emit("leaveRoom", data);
    }

    CodeRefresh(data) {
        this.connect();
        this.socket.emit("codeRefresh", data);
    }

    CodeScroll(data) {
        this.connect();
        this.socket.emit("codeScroll", data);
    }

    ChangeEditRoleReq(data) {
        this.connect();
        this.socket.emit("changeEditRoleReq", data);
    }

    ChangeEditRoleReply(data) {
        this.connect();
        this.socket.emit("changeEditRoleReply", data);
    }

    InviteUserCodeReq(data) {
        this.connect();
        this.socket.emit("inviteUserCodeReq", data);
    }

    InviteUserCodeReply(data) {
        this.connect();
        this.socket.emit("inviteUserCodeReply", data);
    }

    SendChatMessage(msg) {
        this.connect();
        this.socket.emit("sendMessage", msg);
    }

    FiletreeSendUpdate(data) {
        this.connect();
        this.socket.emit("sendFiletreeUpdate", data);
    }

    CodeUpdate(codeContent) {
        this.evtbus.$emit("CodeEdit.CodeUpdate", codeContent);
    }

    CodeEditorScrollUpdate(codeContent) {
        this.evtbus.$emit("CodeEdit.EditorScrollUpdate", codeContent);
    }

    codeEditChangeEditReqRoleServerTransfer(data) {
        this.evtbus.$emit("CodeEdit.ChangeEditRoleReq.ServerTransfer", data);
    }

    codeEditChangeEditRoleReplyServerTransfer(data) {
        this.evtbus.$emit("CodeEdit.ChangeEditRoleReply.ServerTransfer", data);
    }

    codeEditInviteUserCodeReqServerTransfer(data) {
        this.evtbus.$emit("CodeEdit.InviteUserCodeReq.ServerTransfer", data);
    }

    codeEditInviteUserCodeReplyServerTransfer(data) {
        this.evtbus.$emit("CodeEdit.InviteUserCodeReply.ServerTransfer", data);
    }

    codeEditHasUserJoinCodeEdit(usr) {
        this.evtbus.$emit("CodeEdit.HasUserJoinCodeEdit", usr);
    }

    codeEditUserLeaveRoom(data) {
        this.evtbus.$emit(
            "CodeEdit.HasUserQuitEdit",
            data.user,
            data.creatorChangeNotify,
            data.editorChangeNotify,
            data.newUser,
        );
    }

    ReceiveChatMessage(msg) {
        this.evtbus.$emit("CodeChat.ReceiveMessage", msg);
    }

    getCodeUsers(users) {
        this.evtbus.$emit("CodeEdit.CodeUsers", users);
    }

    sendCodeLeaveMsg(msg) {
        this.evtbus.$emit("CodeEdit.UserLeave", msg);
    }

    FiletreeUpdate() {
        this.evtbus.$emit("Filetree.Update");
    }

    SendServerMsg(data) {
        this.evtbus.$emit("CodeEdit.HasMsg", data);
    }

    SendServerProjectUserRemoveMsg(data) {
        this.evtbus.$emit("CodeEdit.ProjectUserRemoved", data);
    }

    SendServerProjectRemoveMsg(data) {
        this.evtbus.$emit("CodeEdit.ProjectRemoved", data);
    }

    //   public connect11() {
    //     this.connect();
    //     this.id = this.socket.id;
    //     this.socket.emit("login", this.userName); //监听connect事件
    //   }
}

export default SocketIO;