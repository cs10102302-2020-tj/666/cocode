"use strict";

class Hub {
    constructor() {
        this.__backbones = {};
        this.__evtStacks = [];
        this.__slice = [].slice;
    }

    _remove(array, valueFn) {
        let idx = 0;
        while (idx < array.length) {
            if (array[idx] === valueFn) {
                array.splice(idx, 1);
            } else {
                idx += 1;
            }
        }
    }

    _subscribe(propertyName) {
        return (names, fn) => {
            names.forEach(name => {
                if (!this._has(this.__backbones, name)) {
                    this.__backbones[name] = {
                        ints: [],
                        subs: [],
                    };
                }
                this.__backbones[name][propertyName].push(fn);
            });
        };
    }

    _splitEventName(eventName) {
        const _evtNames = ("" + eventName)
            .split(/[,\s]+/)
            .map(_name => _name.trim())
            .filter(_name => _name !== "");
        return _evtNames;
    }

    _has(obj, key) {
        return Object.prototype.hasOwnProperty.call(obj, key);
    }

    intercept(eventName, fn) {
        const eventNames = this._splitEventName(eventName);
        const interceptFn = this._subscribe("ints");
        interceptFn(eventNames, fn);
    }

    subscribe(eventName, fn) {
        const eventNames = this._splitEventName(eventName);
        const subscribeFn = this._subscribe("subs");
        subscribeFn(eventNames, fn);
    }

    unsubscribe(eventName, unsubscribeFn) {
        const eventNames = this._splitEventName(eventName);
        const _unsubscribeFn = (names, fn) => {
            names.forEach(name => {
                if (fn === void 0) {
                    delete this.__backbones[name];
                    return;
                }
                if (this._has(this.__backbones, name)) {
                    this._remove(this.__backbones[name].ints, fn);
                    this._remove(this.__backbones[name].subs, fn);
                }
            });
        };

        _unsubscribeFn(eventNames, unsubscribeFn);
    }

    // eslint-disable-next-line no-unused-vars
    publish(eventName, ...values) {
        const eventNames = this._splitEventName(eventName);
        const _publicFn = (names, ...args) => {
            if (!names) {
                return;
            }

            let next, wrap;
            let intsQueues;

            const _args = args && args.length >= 1 ? this.__slice.call(args, 0) : [];
            names.forEach(name => {
                if (this.__evtStacks.indexOf(name) >= 0) {
                    return;
                }
                if (!this._has(this.__backbones, name)) {
                    return;
                }
                this.__evtStacks.push(name);
                intsQueues = this.__backbones[name].ints.slice(0);

                wrap = fn => (_next, ...wrapArgs) => {
                    fn.apply(null, wrapArgs);
                    return _next.apply(null, wrapArgs);
                };

                intsQueues.push.apply(
                    intsQueues,
                    (refSubscribeFns => refSubscribeFns.map(_subscribeFn => wrap(_subscribeFn)))(
                        this.__backbones[name].subs,
                    ),
                );

                next = (...nextFuncArgs) => {
                    const _nextArgs =
                        nextFuncArgs.length >= 1 ? this.__slice.call(nextFuncArgs, 0) : [];
                    if (intsQueues.length) {
                        return intsQueues
                            .shift()
                            .apply(null, [next].concat(this.__slice.call(_nextArgs)));
                    }

                    return void 0;
                };

                try {
                    next && next.apply(null, _args);
                } finally {
                    this.__evtStacks.pop();
                }
            });
        };

        const publishArgs = [eventNames].concat(
            arguments.length >= 2 ? this.__slice.call(arguments, 1) : [],
        );
        _publicFn.apply(_publicFn, publishArgs);
    }
}

const EvtBus = new Hub();

module.exports = { EvtBus };
//export default { EvtBus };