"use strict"

const express = require("express");
const router = express.Router();
const { Project } = require("../db/db");



router.post("/", async (req, res) => {
    const condition = {
        $and: [
            { ProjectName: req.body.projectName },
            { Member: { $elemMatch: { name: req.body.newOwner } } },
        ],
    };
    Project.findOne(condition, function (err, doc) {
        if (err) {
            console.log(err);
            res.send({
                status: 'error',
                message: err
            })
        }
        else {
            if (doc) {
                doc.Owner = req.body.newOwner;
                doc.save();
                res.send({
                    status: 'success',
                    message: doc.Owner
                })
            }
            else {
                res.send({
                    status: 'failure',
                    message: "项目不存在或成员不在项目中"
                })
            }
        }
    })
})

module.exports = router;