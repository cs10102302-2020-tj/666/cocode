"use strict";

//用户信息(不完整，仅是测试token的使用，要把验证token的部分拿出来作为一个公用的中间件)
const express = require("express");
const router = express.Router();
const { User } = require("../db/db");

router.get("/", async(req, res) => {
    const user = await User.findOne({
        username: req.body.userName,
    });
    res.send({
        userName: user.username,
        nickName: user.nickName ? user.nickName : "",
        avatar: user.avatar ? user.avatar : "",
        color: user.color ? user.color : "",
    });
});

module.exports = router;
