"use strict";

const express = require("express");
const router = express.Router();
const { User, Project } = require("../db/db");

router.post("/", async (req, res) => {
    const findProject = await Project.findOne({
        ProjectName: req.body.projectName,
    });

    if (!findProject) {
        //console.log(findProject);
        return res.send({
            status: "DELMEMBER_FAILURE",
            message: "项目不存在",
        });
    }

    const findUser = await User.findOne({
        username: req.body.userName,
    });
    if (!findUser) {
        return res.send({
            status: "DELMEMBER_FAILURE",
            message: "用户不存在",
        });
    }

    const condition = {
        $and: [
            { ProjectName: req.body.projectName },
            { Member: { $elemMatch: { name: req.body.userName } } },
        ],
    };
    const opt = { Member: 1, _id: 1, projectName: 1, Owner: 1 };
    Project.findOne(condition, opt, function (err, doc) {
        if (err) {
            console.log(err);
        }
        if (!doc) {
            return res.send({
                status: "DELMEMBER_FAILURE",
                message: "用户不存在于此项目中",
            });
        }
        console.log(doc);
        console.log(req.body.userName);
        if (doc.Owner == req.body.userName) {
            return res.send({
                status: "DELMEMBER_FAILURE",
                message: "用户为项目拥有者，不可删除此用户",
            });
        }
        else {
            const con = { ProjectName: req.body.projectName };
            const oper = { $pull: { Member: { name: req.body.userName } } };
            Project.updateOne(con, oper, function (err, raw) {
                if (err) {
                    console.log(err);
                }
                if (raw) {
                    console.log(raw);
                }
            });
            User.updateOne({ username: req.body.userName }, { $pull: { Projects: { projectname: req.body.projectName } } },
                function (err, raw) {
                    if (err) {
                        console.log(err);
                    }
                    if (raw) {
                        console.log(raw);
                    }
                },
            );
            res.send({
                status: "DELMEMBER_SUCCESS",
            });
        }
    });

    console.log(req.body);
});

module.exports = router;
