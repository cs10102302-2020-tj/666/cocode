"use strict";

//登录
const express = require("express");
const router = express.Router();
const { User } = require("../db/db");
const { SessionId } = require("../secret");
// const SECRET = require('../secret');
const md5 = require('md5-node');

/* login */
router.post("/", async(req, res) => {
    console.log(req.body);
    const user = await User.findOne({
        username: req.body.userName,
    });
    if (!user) {
        return res.send({
            status: "NAME_PASSWORD_WRONG",
            message: "用户名不存在",
        });
    }
    const isPasswordValid = (md5(req.body.password) === user.password);
    if (!isPasswordValid) {
        return res.send({
            status: "NAME_PASSWORD_WRONG",
            message: "密码错误",
        });
    }

    // req.session.regenerate(function(err) {
    //     if (err) {
    //         return res.send({
    //             status: "GET_ERROR",
    //         });
    //     }

    //     req.session.loginUser = user.userName;
    //     console.log("login ok: -------", user, req.session.loginUser);
    //     res.send({
    //         status: "GET_SUCCESS",
    //         user,
    //     });
    // });

    req.session.loginUser = user.username;
    // console.log("login ok: -------", user, req.session.loginUser);
    res.send({
        status: "GET_SUCCESS",
        user: {
            userName: user.username,
            nickName: user.nickName ? user.nickName : "",
            avatar: user.avatar ? user.avatar : "",
            color: user.color ? user.color : "",
        },
    });

    // authorization token 机制用的比较少了
    // 生成token
    // const jwt = require("jsonwebtoken");
    // const token = jwt.sign({
    //         userName: String(user.username),
    //     },
    //     SECRET,
    // );

    // res.send({
    //     status: "GET_SUCCESS",
    //     user,
    //     token,
    // });
});

router.post("/logout", async(req, res) => {
    req.session.destroy(function(err) {
        if (err) {
            res.send({
                status: "LOGOUT_ERROR",
            });
            return;
        }

        // req.session.loginUser = null;
        res.clearCookie(SessionId);
        res.redirect("/");
    });
});

module.exports = router;