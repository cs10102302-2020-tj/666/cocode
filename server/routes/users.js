"use strict";

//用户列表
const express = require("express");
const router = express.Router();
const { User } = require("../db/db");

/* GET users listing. */
router.get("/", async(req, res) => {
    const users = await User.find();
    res.send(
        users.map(user => {
            return {
                userName: user.username,
                nickName: user.nickName,
                avatar: user.avatar,
                color: user.color,
            };
        }),
    );
});

module.exports = router;
