"use strict"

const express = require("express");
const router = express.Router();
const { User } = require("../db/db");

router.post("/", async (req, res) => {
    const condition = {
        $and: [
            { username: { $regex: req.body.username } },
            {
                Projects: { $not: { $elemMatch: { projectname: req.body.projectName } } }
            }
        ]
    };
    const opt = { username: 1, _id: 0 };
    User.find(condition, opt, function (err, docs) {
        if (err)
            console.log(err);
        console.log(docs);
        var userlist = [];
        for (var i = 0; i < docs.length; i++) {
            userlist.push(docs[i].username);
        }

        console.log(userlist);
        res.send(
            {
                status: "OK",
                userlist
            }
        )
    })
})

module.exports = router;