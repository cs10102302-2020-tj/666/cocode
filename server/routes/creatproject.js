"use strict";

const express = require("express");
const router = express.Router();
const settings = require("../settings");
const { User, Project } = require("../db/db");
const fs = require("fs");

router.post("/", async(req, res) => {
    if (req.body.projectName.length === 0 || req.body.projectName.length > 30) {
        return res.send({
            status: "Creat_Failure",
            message: "项目名称输入不对",
        });
    }

    fs.mkdir(settings.baseProjectPath, (err) => {
        console.log(err);
    })
    const findProject = await Project.findOne({
        ProjectName: req.body.projectName,
    });
    if (findProject) {
        console.log(findProject);
        return res.send({
            status: "PROJECTNAME_DUPLICATE",
            message: "项目名已存在",
        });
    }

    const findUser = await User.findOne({
        username: req.body.owner,
    });
    if (!findUser) {
        return res.send({
            status: "PROJECTNAME_DUPLICATE",
            message: "用户不存在",
        });
    }

    const project = await Project.create({
            ProjectName: req.body.projectName,
            ProjectDesc: req.body.projectDesc ? req.body.projectDesc : "",
            Owner: req.body.owner,
        },
        function(err, docs) {
            docs.Member.push({
                name: req.body.owner,
            });
            docs.save();
            findUser.Projects.push({
                projectname: req.body.projectName,
            });
            findUser.save();
            console.log(req.body);
            const path = settings.baseProjectPath + req.body.projectName;
            console.log(path);
            fs.mkdir(path, err => {
                if (err) {
                    console.log(err);
                    res.send({
                        status: "Creat_Failure",
                        project,
                    });
                } else {
                    res.send({
                        status: "Creat_SUCCESS",
                        project,
                    });
                }
            });
        },
    );
});

module.exports = router;