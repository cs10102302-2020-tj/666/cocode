"use strict";

const socket_io = require("socket.io");
const { EvtBus } = require("../utils");
const cm = require("../cocodeManage");
const FileMan = require("../routes/fileMan");
//const _ = require("lodash");

class SocketIO {
    constructor(server) {
        if (!server) {
            return;
        }

        this.server = server;
        this.connected = false;
        this.state = false;
        this.io = null;
        //this.roomName = "";

        const self = this;
        EvtBus.subscribe("CodeEdit.SendNotifyMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditMsg", {
                roomId: data.roomId,
                userName: data.userName,
                message: data.msg,
                isAllMsg: data.isAllMsg,
            });
        });

        EvtBus.subscribe("CodeEdit.SendProjectUserRemoveMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditProjectUserRemoveMsg", {
                roomId: data.roomId,
                userName: data.userName,
                message: data.msg
            });
        });

        EvtBus.subscribe("CodeEdit.SendProjectRemoveMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditProjectRemoveMsg", {
                roomId: data.roomId,
                message: data.msg
            });
        });
    }

    init(server) {
        this.server = server;
        this.connected = false;
    }

    getRoomUsers(room) {
        if (!room) {
            return [];
        }

        // 获取房间客户端
        const socketRoom = this.io.sockets.adapter.rooms[room];

        if (!socketRoom) {
            return [];
        }

        let userList = [];

        for (const clientId in socketRoom.sockets) {
            console.log("client: %s", clientId); //Seeing is believing
            const client_socket = this.io.sockets.connected[clientId]; //Do whatever you want with this
            userList.push(client_socket.userName);
        }

        return userList;
    }

    start() {
        if (this.connected) {
            return;
        }

        if (!this.server) {
            console.log(`server is null, can't communication`);
            return;
        }

        this.io = socket_io.listen(this.server);
        this.connected = true;
        console.log("socketIO 创建成功");

        this.state = "SocketOK";

        const self = this;
        self.io.on("connection", function(socket) {
            console.log("a user connected", socket.room, socket.id);

            socket.on("codeEditSaveFile", function(data) {
                const socketRoom = socket.room;
                if (!data.roomId || !data.userName) {
                    return;
                }

                const _room = cm.inRoom(data.userName);
                if (!_room || _room.roomId !== data.roomId) {
                    self.io.to(socketRoom).emit("codeEditMsg", {
                        roomId: data.roomId,
                        userName: data.userName,
                        message: "你是谁，从哪里来，想干啥呢！！！",
                        isAllMsg: false
                    });
                    return;
                }

                if (data.content.length > 400000) {
                    self.io.to(data.roomId).emit("codeEditMsg", {
                        roomId: data.roomId,
                        userName: data.userName,
                        message: "文件太大了，保存失败，你想上传斗破苍穹吗！！！",
                        isAllMsg: false
                    });
                    return;
                }

                FileMan.writeFile(data.filePath, data.content, () => {
                    self.io.to(data.roomId).emit("codeEditMsg", {
                        roomId: data.roomId,
                        userName: data.userName,
                        message: "文件保存成功",
                        isAllMsg: true
                    });
                }, () => {});
            });

            socket.on("userTouch", function(data) {
                //console.log("userTouch 1: ", data);

                //socket.userName = data.userName;
                //socket.room = data.roomId;

                //socket.join(socket.room);
                // cm.addTouchUserInfo(socket.userName, socket.room);
                //console.log("userTouch 2: ", cm.getTouchUserRoomId(socket.userName), socket.room, socket.id, data);

                // console.log(cm.getTouchUserRoomId(socket.userName));

                // console.log("userTouch socket: ", socket.room, socket.id, data);
                EvtBus.publish("User.Touch", data);
            });

            socket.on("codeEditUserTouch", function(data) {
                // console.log("userTouch 1: ", data);

                socket.userName = data.userName;
                socket.room = data.roomId;

                socket.join(socket.room);
                cm.addTouchUserInfo(socket.userName, socket.room);

                // console.log("userTouch 2: ", cm.getTouchUserRoomId(socket.userName), socket.room, socket.id, data);
                // EvtBus.publish("User.Touch", data);
            });

            //加入结对编程房间
            socket.on("join", function(data) {
                socket.userName = data.userName;
                socket.room = data.room;

                // console.log("a user join-------begin");

                socket.join(socket.room);
                // self.roomName = socket.room;
                // console.log("user join: ", socket.room, data.userName);

                cm.addRoomUser(socket.room, data.userName);

                const userList = cm.getRoomUsers(socket.room);
                // console.log("users count: ", userList, userList.length);

                socket.broadcast.in(socket.room).emit("othersJoin", socket.userName);
                socket.broadcast.in(socket.room).emit("userList", userList);
                // console.log("a user join-------end");

                //this.io.to(socket.room).emit("othersJoin", socket.userName);
                //this.io.to(socket.room).emit("userList", userList);
            });

            // 发送消息
            socket.on("sendMessage", function(data) {
                // console.log("sendMessage:", data);
                //data.mine = false;
                //socket.broadcast.in(socket.room).emit("receiveMessage", data);
                self.io.to(data.roomId).emit("receiveMessage", data);
            });

            socket.on("leaveRoom", function(userName) {
                // console.log("leaveRoom", userName);
                // console.log("a user leaveRoom-------begin");
                // todo: 暂时不对cache进行处理
                // cm.removeRoomUser(socket.room, userName);
                // const userList = cm.getRoomUsers(socket.room);
                // console.log("users count: ", userList, userList.length);
                // console.log("a user leaveRoom-------end");
            });

            // 断开连接
            socket.on("disconnect", function(arg) {
                //this.io.to(socket.room).emit("othersLeave", socket.userName);
                // console.log("a user disconnected-------begin", arg);
                //cm.removeRoomUser(socket.room, userName);

                // const userList = cm.getRoomUsers(socket.room);
                // console.log("users count: ", userList, userList.length);
                // console.log("a user disconnected-------end", arg);

                socket.broadcast.in(socket.room).emit("othersLeave", socket.userName);
            });

            // 编辑器代码更新同步
            socket.on("codeRefresh", function(code) {
                self.asyncCode(code);
            });

            // 编辑器滚动同步
            socket.on("codeScroll", function(code) {
                self.asyncScroll(code);
            });

            // 切换编辑人员角色申请
            socket.on("changeEditRoleReq", function(data) {
                self.changeEditRoleReqTransfer(data);
            });

            // 切换编辑人员角色回复
            socket.on("changeEditRoleReply", function(data) {
                self.changeEditRoleReplyTransfer(data);
            });

            // 切换编辑人员角色申请
            socket.on("inviteUserCodeReq", function(data) {
                self.inviteUserCodeReqTransfer(data);
            });

            // 切换编辑人员角色回复
            socket.on("inviteUserCodeReply", function(data) {
                self.inviteUserCodeReplyTransfer(data);
            });

            // 加入房间通知
            socket.on("codeEditNotifyUserJoinReq", function(data) {
                self.codeEditNotifyUserJoinReq(data);
            });

            socket.on("codeEditUserLeaveRoomReq", function(data) {
                self.codeEditUserLeaveRoomReq(data);
            });

            socket.on("sendFiletreeUpdate", function(data) {
                self.FiletreeUpdateTransfer(data);
            });
        });

        EvtBus.subscribe("CodeEdit.SendNotifyMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditMsg", {
                roomId: data.roomId,
                userName: data.userName,
                message: data.msg,
                isAllMsg: data.isAllMsg,
            });
        });

        EvtBus.subscribe("CodeEdit.SendProjectUserRemoveMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditProjectUserRemoveMsg", {
                roomId: data.roomId,
                projectName: data.projectName,
                userName: data.userName,
                message: data.msg
            });
        });

        EvtBus.subscribe("CodeEdit.SendProjectRemoveMsg", (data) => {
            self.io.to(data.roomId).emit("codeEditProjectRemoveMsg", {
                roomId: data.roomId,
                projectName: data.projectName,
                userName: data.userName,
                message: data.message,
            });
        });
    }

    stop() {
        this.io.close();
        this.connected = false;
    }

    asyncCode(codeContent) {
        if (!this.connected) {
            return;
        }

        cm.setRoomFileContent(codeContent.roomId, codeContent.content);
        this.io.to(codeContent.roomId).emit("codeUpdate", codeContent);
    }

    asyncScroll(codeContent) {
        if (!this.connected) {
            return;
        }

        this.io.to(codeContent.roomId).emit("editorScrollUpdate", codeContent);
    }

    changeEditRoleReqTransfer(data) {
        if (!this.connected) {
            return;
        }

        this.io.to(data.roomId).emit("changeEditRoleReqTransfer", data);
    }

    changeEditRoleReplyTransfer(data) {
        if (!this.connected) {
            return;
        }

        if (data.confirm) {
            // console.log("room: ", data.roomId, " editor: ", data.reqUserName);
            cm.setRoomEditor(data.roomId, data.reqUserName);
        }
        this.io.to(data.roomId).emit("changeEditRoleReplyTransfer", data);
    }

    inviteUserCodeReqTransfer(data) {
        if (!this.connected) {
            return;
        }

        // console.log("invite req", data);
        const invitedUserRoomId = cm.getTouchUserRoomId(data.inviteUserName);
        // console.log("roomId: ", data.roomId, "invitedUser roomId: ", invitedUserRoomId);
        this.io.to(invitedUserRoomId).emit("inviteUserCodeReqTransfer", data);
    }

    inviteUserCodeReplyTransfer(data) {
        if (!this.connected) {
            return;
        }

        this.io.to(data.roomId).emit("inviteUserCodeReplyTransfer", data);
    }

    codeEditNotifyUserJoinReq(data) {
        if (!this.connected) {
            return;
        }

        this.io.to(data.roomId).emit("codeEditNotifyUserJoinReqTransfer", data);
    }

    codeEditUserLeaveRoomReq(data) {
        if (!this.connected) {
            return;
        }

        this.io.to(data.roomId).emit("codeEditUserLeaveRoomReqTransfer", data);
    }

    FiletreeUpdateTransfer(data) {
        if (!this.connected) {
            return;
        }

        const projectUsers = data.projectUsers;
        for (let i = 0; i < projectUsers.length; i++) {
            this.io.to(cm.getTouchUserRoomId(projectUsers[i].userName)).emit("filetreeUpdateTransfer");
        }
    }
}

module.exports = new SocketIO();