"use strict";

//mongodb数据库，用mongoose对其进行操作（数据库未放入项目）
const mongoose = require("mongoose");
const md5 = require("md5-node");
const settings = require("../settings");

//连接数据库，后面3个参数要是不加会waring
mongoose.connect(settings.databaseUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
});
const memberName = new mongoose.Schema({
    name: String,
});
const projectName = new mongoose.Schema({
    projectname: String,
});
//项目组
const ProjectMessage = new mongoose.Schema({
    ProjectName: { type: String, unique: true },
    ProjectDesc: { type: String, default: "" },
    CreatDate: { type: Date, default: Date.now },
    Owner: { type: String },
    Member: [memberName],
});

//User表的参数，密码用md5进行加密
const UserSchema = new mongoose.Schema({
    username: { type: String, unique: true },
    password: {
        type: String,
        set(val) {
            return md5(val);
        }
    },
    nickName: { type: String },
    avatar: { type: String },
    color: { type: String },
    Projects: [projectName], //同类型表格
});

//创建User表
const User = mongoose.model("User", UserSchema);
const Project = mongoose.model("Project", ProjectMessage);
const mongooseConn = mongoose.connection;
module.exports = { User, Project, mongooseConn };
